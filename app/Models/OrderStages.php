<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStages extends Model
{
    use HasFactory;

    public function item(){
        return $this->hasMany(Order::class,'order_stage_id','id');
    }
}
