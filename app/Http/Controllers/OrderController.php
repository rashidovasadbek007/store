<?php

namespace App\Http\Controllers;

class OrderController extends Controller
{
    public function index()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'kanban-application',
        ];
        return view('store.order.index', ['pageConfigs' => $pageConfigs]);
    }
}
