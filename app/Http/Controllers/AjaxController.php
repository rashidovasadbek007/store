<?php

namespace App\Http\Controllers;

use App\Models\OrderStages;

class AjaxController extends Controller
{

    public function kanbanDataList()
    {
        $order_stages = OrderStages::with('item')->get();
        return response()->json($order_stages);
    }

}
