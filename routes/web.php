<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\UserInterfaceController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\MiscellaneousController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\ChartsController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\StoreSettingsController;
use App\Http\Controllers\AjaxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/* Route Icons */
Route::group(['prefix' => 'icons'], function () {
    Route::get('feather', [UserInterfaceController::class, 'icons_feather'])->name('icons-feather');
});
/* Route Icons */

/* Modal Examples */
Route::get('/modal-examples', [PagesController::class, 'modal_examples'])->name('modal-examples');

/* Route Pages */
Route::get('/error', [MiscellaneousController::class, 'error'])->name('error');

/* Route Authentication Pages */
Route::group(['prefix' => 'auth','middleware' => 'guest'], function () {
    Route::controller(AuthenticationController::class)->group(function (){
        Route::post('logout','logout')->name('logout')->withoutMiddleware('guest');
        Route::get('login-basic', 'login_basic')->name('login');
        Route::get('login-cover', 'login_cover')->name('auth-login-cover');
        Route::get('register-basic','register')->name('register');
        Route::get('register-cover',  'register_cover')->name('auth-register-cover');
        Route::post('register',  'submitRegister')->name('submit-register');
        Route::get('forgot-password-basic',  'forgot_password_basic')->name('auth-forgot-password-basic');
        Route::get('forgot-password-cover',  'forgot_password_cover')->name('auth-forgot-password-cover');
        Route::get('reset-password-basic',  'reset_password_basic')->name('auth-reset-password-basic');
        Route::get('reset-password-cover',  'reset_password_cover')->name('auth-reset-password-cover');
        Route::get('verify-email-basic',  'verify_email_basic')->name('auth-verify-email-basic');
        Route::get('verify-email-cover',  'verify_email_cover')->name('auth-verify-email-cover');
        Route::get('two-steps-basic',  'two_steps_basic')->name('auth-two-steps-basic');
        Route::get('two-steps-cover',  'two_steps_cover')->name('auth-two-steps-cover');
        Route::get('register-multisteps',  'register_multi_steps')->name('auth-register-multisteps');
        Route::get('lock-screen',  'lock_screen')->name('auth-lock_screen');
    });
});

/* Route Authentication Pages */

// map leaflet
Route::get('/maps/leaflet', [ChartsController::class, 'maps_leaflet'])->name('map-leaflet');

// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);


//Route Store

/* Route Store Pages */
Route::group(['prefix' => '/','middleware' => 'auth'], function () {
    Route::get('/purchase', [PurchaseController::class, 'index'])->name('index');
    Route::get('order', [OrderController::class, 'index'])->name('index');
    Route::get('settings', [StoreSettingsController::class, 'index'])->name('settings.index');
});

Route::group(['prefix' => 'ajax'], function () {
    Route::get('/kanban-data-list',[AjaxController::class,'kanbanDataList'])
        ->name('kanban-data-list');
});

/* Route Store Pages */

