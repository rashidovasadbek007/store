@extends('layouts/contentLayoutMaster')

@section('title', 'Sotib olish')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')
    <!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Rasm</th>
                            <th>Mahsulot</th>
                            <th>Ta'minotchi</th>
                            <th>Summa</th>
                            <th>To'lov turi</th>
                            <th>Miqdori</th>
                            <th>Qoldiq</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th><img src="/images/iphone.jpg" alt=""
                                     style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Iphone 11</th>
                            <th>Rashidov Asadbek</th>
                            <th>7 650 000</th>
                            <th>Click</th>
                            <th>100</th>
                            <th>33</th>
                        </tr>
                        <tr>
                            <th><img src="/images/mi9.jpg" alt=""
                                     style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Xiaomi note 9s</th>
                            <th>Pirimqulov Akmal</th>
                            <th>2 680 000</th>
                            <th>Payme</th>
                            <th>130</th>
                            <th>67</th>
                        </tr>
                        <tr>
                            <th><img src="/images/s22.jpg" alt=""
                                     style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Samsung Galaxy S 22 Ultra</th>
                            <th>Nurillo Ismoilov</th>
                            <th>7 860 000</th>
                            <th>Payme</th>
                            <th>80</th>
                            <th>24</th>
                        </tr>
                        <tr>
                            <th><img src="/images/air3.webp" alt=""
                                     style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>

                            <th>Air pods 3</th>
                            <th>Boypo'latov Shohjahon</th>
                            <th>150 000</th>
                            <th>Click</th>
                            <th>350</th>
                            <th>178</th>
                        </tr>
                        <tr>
                            <th><img src="/images/a12.jpg" alt="" style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Samsung Galaxy A 12</th>
                            <th>Abdukarimov Jamol</th>
                            <th>1 750 000</th>
                            <th>Payme</th>
                            <th>150</th>
                            <th>88</th>
                        </tr>
                        <tr>
                            <th><img src="/images/mipad.jpg" alt="" style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Xiaomi pad</th>
                            <th>Aliyev Sarvar</th>
                            <th>5 780 000</th>
                            <th>Click</th>
                            <th>50</th>
                            <th>38</th>
                        </tr>
                        <tr>

                            <th><img src="/images/goodwell.jpg" alt="" style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th><th>Goodwell GW 0845 X</th>
                            <th>G'aniyeva Malika</th>
                            <th>5 040 000</th>
                            <th>Click</th>
                            <th>15</th>
                            <th>8</th>
                        </tr>
                        <tr>
                            <th><img src="/images/mi11.png" alt="" style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Xiaomi Redmi Note 11</th>
                            <th>2 250 000</th>
                            <th>Ilhomov Asqarali</th>
                            <th>Payme</th>
                            <th>50</th>
                            <th>38</th>
                        </tr>
                        <tr>
                            <th><img src="/images/tabs7.jpg" alt="" style="width: 50px; height: 50px; border-radius: 50%; object-fit: contain;"></th>
                            <th>Samsung Galaxy Tab S7+</th>
                            <th>7 180 000</th>
                            <th>Sodiqova Nargiza</th>
                            <th>Payme</th>
                            <th>10</th>
                            <th>7</th>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Rasm</th>
                            <th>Mahsulot</th>
                            <th>Ta'minotchi</th>
                            <th>Summa</th>
                            <th>To'lov turi</th>
                            <th>Miqdori</th>
                            <th>Qoldiq</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal modal-slide-in fade" id="modals-slide-in">
            <div class="modal-dialog sidebar-sm">
                <form class="add-new-record modal-content pt-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title" id="exampleModalLabel">New Record</h5>
                    </div>
                    <div class="modal-body flex-grow-1">
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-fullname">Full Name</label>
                            <input
                                type="text"
                                class="form-control dt-full-name"
                                id="basic-icon-default-fullname"
                                placeholder="John Doe"
                                aria-label="John Doe"
                            />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-post">Post</label>
                            <input
                                type="text"
                                id="basic-icon-default-post"
                                class="form-control dt-post"
                                placeholder="Web Developer"
                                aria-label="Web Developer"
                            />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-email">Email</label>
                            <input
                                type="text"
                                id="basic-icon-default-email"
                                class="form-control dt-email"
                                placeholder="john.doe@example.com"
                                aria-label="john.doe@example.com"
                            />
                            <small class="form-text"> You can use letters, numbers & periods </small>
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-date">Joining Date</label>
                            <input
                                type="text"
                                class="form-control dt-date"
                                id="basic-icon-default-date"
                                placeholder="MM/DD/YYYY"
                                aria-label="MM/DD/YYYY"
                            />
                        </div>
                        <div class="mb-4">
                            <label class="form-label" for="basic-icon-default-salary">Salary</label>
                            <input
                                type="text"
                                id="basic-icon-default-salary"
                                class="form-control dt-salary"
                                placeholder="$12000"
                                aria-label="$12000"
                            />
                        </div>
                        <button type="button" class="btn btn-primary data-submit me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--/ Basic table -->

@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset(mix('js/scripts/tables/table-datatables-basic.js')) }}"></script>
@endsection
