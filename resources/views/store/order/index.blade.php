@extends('layouts/contentLayoutMaster')

@section('title', 'Order')

@section('vendor-style')
    <!-- Vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/jkanban/jkanban.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/katex.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/monokai-sublime.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.snow.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/editors/quill/quill.bubble.css')) }}">
@endsection
@section('page-style')
    <!-- Page css files -->
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-quill-editor.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('css/base/pages/app-kanban.css')) }}">

    <style>

        .top-title .dot {
            width: 2px;
            height: 2px;
            border-radius: 50%;
            background: #000;
            margin-bottom: 3px;
        }

        .top-title .dot:last-child {
            margin-bottom: 0;
        }

    </style>
@endsection

@section('content')
    <!-- Kanban starts -->
    <section class="app-kanban-wrapper kanban-aplication">
    {{--        <div class="row">--}}
    {{--            <div class="col-12">--}}
    {{--                <form class="add-new-board">--}}
    {{--                    <label class="add-new-btn mb-2" for="add-new-board-input">--}}
    {{--                        <i class="align-middle" data-feather="plus"></i>--}}
    {{--                        <span class="align-middle">Yangi ustun qo'shish</span>--}}
    {{--                    </label>--}}
    {{--                    <input--}}
    {{--                        type="text"--}}
    {{--                        class="form-control add-new-board-input mb-50"--}}
    {{--                        placeholder="Add Board Title"--}}
    {{--                        id="add-new-board-input"--}}
    {{--                        required--}}
    {{--                    />--}}
    {{--                    <div class="mb-1 add-new-board-input">--}}
    {{--                        <button class="btn btn-primary btn-sm me-75">Add</button>--}}
    {{--                        <button type="button" class="btn btn-outline-secondary btn-sm cancel-add-new">Cancel</button>--}}
    {{--                    </div>--}}
    {{--                </form>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    <!-- Kanban content starts -->
        <div class="kanban-wrapper mt-3">
            <div class="kanban-container">
                <div class="canban-item" style="width: 250px; margin-right: 30px">
                    <div class="top-title d-flex align-items-center justify-content-between mb-2">
                        <h4 class="mb-0">Yangi</h4>
                        <div class="d-flex flex-column">
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/iphone.jpg" height="32">
                        <p class="mb-0 fw-bold">Iphone 12 pro</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Dinora"
                                    aria-label="Dinora"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Asadbek"
                                    aria-label="Asadbek"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
{{--                        <img class="img-fluid rounded mb-1" src="images/air3.webp" height="32">--}}
                        <img class="img-fluid rounded mb-1" src="images/ipad.jpg" height="32">
                        <p class="mb-0 fw-bold">Ipad pro</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Malika"
                                    aria-label="Malika"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Mansur"
                                    aria-label="Mansur"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/mipad.jpg" height="32">
                        <p class="mb-0 fw-bold">Xiaomi pad</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/s22.jpg" height="32">
                        <p class="mb-0 fw-bold">Samsung Galaxy S 22 Ultra</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/22ultra.jpg" height="32">
                        <p class="mb-0 fw-bold">Samsung Galaxy S 22 Ultra</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="canban-item" style="width: 250px;  margin-right: 30px">
                    <div class="top-title d-flex align-items-center justify-content-between mb-2">
                        <h4 class="mb-0">Yo'lda</h4>
                        <div class="d-flex flex-column">
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/air3.webp" height="32">
                        <p class="mb-0 fw-bold">Airpods pro</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Dinora"
                                    aria-label="Dinora"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Asadbek"
                                    aria-label="Asadbek"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        {{--                        <img class="img-fluid rounded mb-1" src="images/air3.webp" height="32">--}}
                        <img class="img-fluid rounded mb-1" src="images/mi11.png" height="32">
                        <p class="mb-0 fw-bold">Xiaomi 11</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Malika"
                                    aria-label="Malika"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Mansur"
                                    aria-label="Mansur"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/mipad.jpg" height="32">
                        <p class="mb-0 fw-bold">Xiaomi pad</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/22ultra.jpg" height="32">
                        <p class="mb-0 fw-bold">Samsung Galaxy S 22 Ultra</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="canban-item" style="width: 250px;  margin-right: 30px">
                    <div class="top-title d-flex align-items-center justify-content-between mb-2">
                        <h4 class="mb-0">Yetkazildi</h4>
                        <div class="d-flex flex-column">
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/mipad.jpg" height="32">
                        <p class="mb-0 fw-bold">Xiaomi pad</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/air3.webp" height="32">
                        <p class="mb-0 fw-bold">Airpods pro</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Dinora"
                                    aria-label="Dinora"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Asadbek"
                                    aria-label="Asadbek"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        {{--                        <img class="img-fluid rounded mb-1" src="images/air3.webp" height="32">--}}
                        <img class="img-fluid rounded mb-1" src="images/mi11.png" height="32">
                        <p class="mb-0 fw-bold">Xiaomi 11</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Malika"
                                    aria-label="Malika"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Mansur"
                                    aria-label="Mansur"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div class="canban-item" style="width: 250px;  margin-right: 30px">
                    <div class="top-title d-flex align-items-center justify-content-between mb-2">
                        <h4 class="mb-0">Qaytarildi</h4>
                        <div class="d-flex flex-column">
                            <div class="dot"></div>
                            <div class="dot"></div>
                            <div class="dot"></div>
                        </div>
                    </div>

                    <div class="kanban-column bg-white rounded p-2 mb-2">
                        <h4>Zakaz</h4>
                        <img class="img-fluid rounded mb-1" src="images/22ultra.jpg" height="32">
                        <p class="mb-0 fw-bold">Samsung Galaxy S 22 Ultra</p>
                        <div class="d-flex justify-content-between align-items-center flex-wrap mt-1">
                            <div><span class="align-middle me-50"><svg xmlns="http://www.w3.org/2000/svg" width="24"
                                                                       height="24" viewBox="0 0 24 24" fill="none"
                                                                       stroke="currentColor" stroke-width="2"
                                                                       stroke-linecap="round" stroke-linejoin="round"
                                                                       class="feather feather-paperclip font-medium-1 align-middle me-25"><path
                                            d="M21.44 11.05l-9.19 9.19a6 6 0 0 1-8.49-8.49l9.19-9.19a4 4 0 0 1 5.66 5.66l-9.2 9.19a2 2 0 0 1-2.83-2.83l8.49-8.48"></path></svg><span
                                        class="attachments align-middle">2</span></span> <span class="align-middle"><svg
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round"
                                        class="feather feather-message-square font-medium-1 align-middle me-25"><path
                                            d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg><span>1</span></span>
                            </div>
                            <ul class="avatar-group mb-0">
                                <li class="avatar kanban-item-avatar  pull-up  me-0" data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Laurel"
                                    aria-label="Laurel"><img
                                        src="/images/portrait/small/avatar-s-3.jpg" alt="Avatar"
                                        height="28"></li>
                                <li class="avatar kanban-item-avatar  pull-up " data-bs-toggle="tooltip"
                                    data-bs-placement="top" title="" data-bs-original-title="Oliver"
                                    aria-label="Oliver"><img
                                        src="images/portrait/small/avatar-s-4.jpg" alt="Avatar"
                                        height="28"></li>
                            </ul>
                        </div>
                    </div>
                </div>
                        <div class="canban-item">
                            <div class="col-12">
                                <form class="add-new-board mt-0">
                                    <label class="add-new-btn mb-2" for="add-new-board-input">
                                        <i class="align-middle" data-feather="plus"></i>
                                        <span class="align-middle">Yangi ustun qo'shish</span>
                                    </label>
                                </form>
                            </div>
                        </div>
            </div>
        </div>
        <!-- Kanban content ends -->
        <!-- Kanban Sidebar starts -->
        <div class="modal modal-slide-in update-item-sidebar fade">
            <div class="modal-dialog sidebar-lg">
                <div class="modal-content p-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title">Update Item</h5>
                    </div>
                    <div class="modal-body flex-grow-1">
                        <ul class="nav nav-tabs tabs-line">
                            <li class="nav-item">
                                <a class="nav-link nav-link-update active" data-bs-toggle="tab" href="#tab-update">
                                    <i data-feather="edit"></i>
                                    <span class="align-middle">Update</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-link-activity" data-bs-toggle="tab" href="#tab-activity">
                                    <i data-feather="activity"></i>
                                    <span class="align-middle">Activity</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content mt-2">
                            <div class="tab-pane tab-pane-update fade show active" id="tab-update" role="tabpanel">
                                <form class="update-item-form">
                                    <div class="mb-1">
                                        <label class="form-label" for="title">Title</label>
                                        <input type="text" id="title" class="form-control" placeholder="Enter Title"/>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="due-date">Due Date</label>
                                        <input type="text" id="due-date" class="form-control"
                                               placeholder="Enter Due Date"/>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label" for="label">Label</label>
                                        <select class="select2 select2-label form-select" id="label">
                                            <option value="">&nbsp;</option>
                                            <option data-color="badge-light-success" value="UX">UX</option>
                                            <option data-color="badge-light-warning" value="Images">Images</option>
                                            <option data-color="badge-light-info" value="App">App</option>
                                            <option data-color="badge-light-danger" value="Code Review">Code Review
                                            </option>
                                            <option data-color="badge-light-success" value="Forms">Forms</option>
                                            <option data-color="badge-light-primary" value="Charts & Maps">Charts &
                                                Maps
                                            </option>
                                        </select>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label">Assigned</label>
                                        <ul class="assigned ps-0"></ul>
                                    </div>
                                    <div class="mb-1">
                                        <label for="attachments" class="form-label">Attachments</label>
                                        <input class="form-control file-attachments" type="file" id="attachments"
                                               multiple/>
                                    </div>
                                    <div class="mb-1">
                                        <label class="form-label">Comment</label>
                                        <div class="comment-editor border-bottom-0"></div>
                                        <div class="d-flex justify-content-end comment-toolbar">
                    <span class="ql-formats me-0">
                      <button class="ql-bold"></button>
                      <button class="ql-italic"></button>
                      <button class="ql-underline"></button>
                      <button class="ql-link"></button>
                      <button class="ql-image"></button>
                    </span>
                                        </div>
                                    </div>
                                    <div class="mb-1">
                                        <div class="d-flex flex-wrap">
                                            <button class="btn btn-primary me-1" data-bs-dismiss="modal">Save</button>
                                            <button type="button" class="btn btn-outline-danger"
                                                    data-bs-dismiss="modal">Delete
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane tab-pane-activity pb-1 fade" id="tab-activity" role="tabpanel">
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar bg-light-success my-0 ms-0 me-50">
                                        <span class="avatar-content">HJ</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Jordan</span> Left the board.</p>
                                        <small class="text-muted">Today 11:00 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-6.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Dianna</span> mentioned <span
                                                class="fw-bold text-primary">@bruce</span> in a
                                            comment.
                                        </p>
                                        <small class="text-muted">Today 10:20 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-2.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Martian</span> added moved Charts & Maps task to the
                                            done board.
                                        </p>
                                        <small class="text-muted">Today 10:00 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-1.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Barry</span> Commented on App review task.
                                        </p>
                                        <small class="text-muted">Today 8:32 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar bg-light-dark my-0 ms-0 me-50">
                                        <span class="avatar-content">BW</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Bruce</span> was assigned task of code
                                            review.</p>
                                        <small class="text-muted">Today 8:30 PM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar bg-light-danger my-0 ms-0 me-50">
                                        <span class="avatar-content">CK</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Clark</span> assigned task UX Research to
                                            <span class="fw-bold text-primary">@martian</span>
                                        </p>
                                        <small class="text-muted">Today 8:00 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-4.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Ray</span> Added moved <span class="fw-bold">Forms & Tables</span>
                                            task from
                                            in progress to done.
                                        </p>
                                        <small class="text-muted">Today 7:45 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-1.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Barry</span> Complete all the tasks
                                            assigned to him.</p>
                                        <small class="text-muted">Today 7:17 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar bg-light-success my-0 ms-0 me-50">
                                        <span class="avatar-content">HJ</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Jordan</span> added task to update new
                                            images.</p>
                                        <small class="text-muted">Today 7:00 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar my-0 ms-0 me-50">
                                        <img src="{{asset('images/portrait/small/avatar-s-6.jpg')}}" alt="Avatar"
                                             height="32"/>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Dianna</span> moved task <span
                                                class="fw-bold">FAQ UX</span> from in progress
                                            to done board.
                                        </p>
                                        <small class="text-muted">Today 7:00 AM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start mb-1">
                                    <div class="avatar bg-light-danger my-0 ms-0 me-50">
                                        <span class="avatar-content">CK</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0">
                                            <span class="fw-bold">Clark</span> added new board with name <span
                                                class="fw-bold">Done</span>.
                                        </p>
                                        <small class="text-muted">Yesterday 3:00 PM</small>
                                    </div>
                                </div>
                                <div class="d-flex align-items-start">
                                    <div class="avatar bg-light-dark my-0 ms-0 me-50">
                                        <span class="avatar-content">BW</span>
                                    </div>
                                    <div class="more-info">
                                        <p class="mb-0"><span class="fw-bold">Bruce</span> added new task in progress
                                            board.</p>
                                        <small class="text-muted">Yesterday 12:00 PM</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Kanban Sidebar ends -->

    </section>
    <!-- Kanban ends -->
@endsection

@section('vendor-script')
    <!-- Vendor js files -->
    <script src="{{ asset(mix('vendors/js/jkanban/jkanban.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/katex.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/highlight.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/editors/quill/quill.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    {{--    <script src="{{ asset(mix('js/scripts/pages/order-stages.js')) }}"></script>--}}
@endsection
