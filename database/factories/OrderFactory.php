<?php

namespace Database\Factories;

use App\Models\OrderStages;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'order_stage_id' => OrderStages::query()->inRandomOrder()->limit(1)->first()->id
        ];
    }
}
